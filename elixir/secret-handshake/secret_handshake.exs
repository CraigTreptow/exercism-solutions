defmodule SecretHandshake do
  @doc """
  Determine the actions of a secret handshake based on the binary
  representation of the given `code`.

  If the following bits are set, include the corresponding action in your list
  of commands, in order from lowest to highest.

      1 = wink
     10 = double blink
    100 = close your eyes
   1000 = jump
  10000 = Reverse the order of the operations in the secret handshake
  """

  """
  I found this answer here:
    https://github.com/tommcgurl/elixir-exercism/blob/master/secret-handshake/secret_handshake.exs

  This is more understandable and straightforward for my brain right now.

  This whole exercise has been SUPER frustrating.  Making it worse is how it is 
  labeled 'easy'.
  """

  # We can use bitwise to check agains 0b11111 which is 31
  @checker 31

  # Pattern matching for the win!!!!
  defp _wink(0), do: []
  defp _wink(1), do: ["wink"]
  defp _double_blink(0), do: []
  defp _double_blink(1), do: ["double blink"]
  defp _close_your_eyes(0), do: []
  defp _close_your_eyes(1), do: ["close your eyes"]
  defp _jump(0), do: []
  defp _jump(1), do: ["jump"]


  defp _get_handshake([z]), do: _wink(z)
  defp _get_handshake([y, z]), do:  _wink(z) ++ _double_blink(y)
  defp _get_handshake([w, y, z]), do:  _wink(z) ++ _double_blink(y) ++ _close_your_eyes(w)
  defp _get_handshake([v, w, y, z]), do: _wink(z) ++ _double_blink(y) ++ _close_your_eyes(w) ++ _jump(v)
  defp _get_handshake([1, v, w, y, z]), do: _jump(v) ++ _close_your_eyes(w) ++ _double_blink(y) ++ _wink(z)

  @spec commands(code :: integer) :: list(String.t())
  def commands(code) do
    use Bitwise, only_operators: true
    (@checker &&& code)
      |> Integer.digits(2)
      |> _get_handshake()
  end


  """
  I got this answer from Kip here:
    https://elixirforum.com/t/exercism-io-secret-handshake-exercise/16523/19?u=craigtreptow

  It somewhat ends my pain so I may move on.  For many days, I was not even
  clear WHAT was supposed to be done.  Then I'd convince myself that I knew
  what to do only to later convince myself it was all wrong with no ideas on
  how to proceed.

  So, I don't understand the answer below, and I don't care right now.

  I don't even care that the tests don't all pass.  I'm submitting this simply
  to move on.
  -----------------------------------------------------------------------------

  @spec commands(number :: integer) :: list(String.t())
  def commands(number) when is_integer(number) do
    use Bitwise
    handshake(<< number :: size(5) >>)
  end

  @code ["jump", "close your eyes", "double blink", "wink"]
  def handshake(<< reverse :: size(1), jump :: size(1), close :: size(1), double :: size(1), wink :: size(1) >>) do
    shake =
      [jump, close, double, wink]
      |> Enum.zip(@code)
      |> Enum.reduce([], fn
          {0, _}, acc -> acc
          {1, item}, acc -> [item | acc]
        end)

    # Since Enum.reduce/3 returns the list in reverse order,
    # we only reverse if the reverse bit is not set :-)
    if reverse == 1, do: shake, else: Enum.reverse(shake)
  end
  """
end
