defmodule Bob do
  def hey(input) do
    cond do
      silence?(input)               -> "Fine. Be that way!"
      asking_in_other_forms?(input) -> "Calm down, I know what I'm doing!"
      asking?(input)                -> "Sure."
      shouting?(input)              -> "Whoa, chill out!"
      true                          -> "Whatever."
    end
  end

  defp silence?(""), do: true
  defp silence?(string) do
    String.match?(string,  ~r(\A\s+\z))
  end

  defp asking?(string) do
    String.ends_with?(string, "?")
  end

  defp asking_in_other_forms?(string) do
    String.ends_with?(string, "?") && shouting?(string)
  end

  defp shouting?(string) do
    string == String.upcase(string) && contains_letter?(string)
  end

  defp contains_letter?(string) do
    # \p{L} or \p{Letter}: any kind of letter from any language.
    String.match?(string, ~r/[\p{L}]/)
  end
end
