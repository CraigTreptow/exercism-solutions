defmodule RotationalCipher do
  @doc """
  Given a plaintext and amount to shift by, return a rotated string.

  Example:
  iex> RotationalCipher.rotate("Attack at dawn", 13)
  "Nggnpx ng qnja"

  I was making it way too hard.  This helped tremendously:
  https://stackoverflow.com/questions/46386066/convert-list-of-codepoints-or-binary-to-string
  """

  @spec rotate(text :: String.t(), shift :: integer) :: String.t()
  def rotate(text, shift_amount) do
    Enum.map(String.to_charlist(text), fn(char) -> perform_shift(char, shift_amount) end)
    |> List.to_string()
  end

  # ?a == 97, ?z == 122
  defp perform_shift(char, shift_amount) when char in ?a..?z do
    rem((char - ?a + shift_amount), 26) + ?a
  end
  defp perform_shift(char, shift_amount) when char in ?A..?Z do
    rem((char - ?A + shift_amount), 26) + ?A
  end
  defp perform_shift(char, _), do: char
end
