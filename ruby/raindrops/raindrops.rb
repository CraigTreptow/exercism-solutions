class Raindrops
  def self.convert(number)
    factors = []
    (1..number/2).each do |n|
      if (number % n == 0)
        factors << n
      end
    end
    factors << number

    answer = ""
    answer += "Pling" if factors.include?(3)
    answer += "Plang" if factors.include?(5)
    answer += "Plong" if factors.include?(7)
    answer += number.to_s if (!factors.include?(7) && !factors.include?(5) && !factors.include?(3))
    return answer
  end
end
