module Bob exposing (..)
import Regex

hey : String -> String
hey something =
  if (Regex.contains(Regex.regex "^\\s+$") something) then
    "Fine. Be that way!"
  else if (String.toUpper something == something) &&
          (String.endsWith "?" something) then
    "Calm down, I know what I'm doing!"
  else if String.toUpper something == something then
    "Whoa, chill out!"
  else if String.endsWith "?" something then
    "Sure."
  else
    "Whatever."
