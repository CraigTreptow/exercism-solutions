module Acronym (abbreviate) where

import qualified Data.Text as T
import           Data.Text (Text)

abbreviate :: Text -> Text
abbreviate phrase =
 T.chunksOf 3 phrase

